
# Jasmine on Node.js

This directory contains a basic project template using [_Node.js_](https://nodejs.org/) and [_Jasmine_](https://jasmine.github.io/).

To get hacking inside Docker:

```bash
docker run -it -v "$PWD:/app" -w /app node:latest bash -l
```

To install dependencies:

```bash
npm install
```

To run the test suite:

```bash
npm test
```

Happy hacking and debugging! :-)

Note: After switching between _Node.js_ versions, `npm test` can result in an error. Removing the `node_modules` directory (with `rm -rf node_modules`) should fix it.



## Requirements

### Requirement 1
Basically I would like to create new game_score with X players each having some starting credit amount. 
To this game, I would like to be able to add or substract credits from a player, and provide a reason for the action. 
I would also want to be able to get all players in the game with their sums, and sums of individual players.
Trying to go to negative should yield an error. Updating a nonexistent player, also an error.


### Requirement 2
I might wanna also create a game_round_score of a game_score. I could create a gameround from the game, and then update the game_score with a game_round_score.
Game_round could be updated with the game_round_score, causing credits to go up with one player and down with other players.
People should not be able to go to negative in the game_round_score either.