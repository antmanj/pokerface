const Table = require('../src/Table');
const Player = require('../src/Player');

describe('Table', () => {
  describe('gather players', () => {
    it('adds a player', () => {
      const table = new Table();

      const player = new Player();
      table.join(player);

      expect(table.players()).toContain(player);
    });

    it('adds 2 players one by one',  () => {
      const table = new Table();
      const player1 = new Player();
      const player2 = new Player();

      table.join(player1);
      table.join(player2);


      expect(table.players()).toEqual([player1, player2]);
    });

    it('adds credit to a player', () => {
      const table = new Table();

      const player = new Player();
      table.join(player);
      table.addCredit(player, 95);

      expect(table.creditFor(player)).toBe(95);
    });
  });
});
