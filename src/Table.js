class Table {
  constructor() {
    this._players = [];
    this._transactions = [];
  }

  join(player) {
    this._players.push(player);
  }

  addCredit(player, credit) {
    this._transactions.push({player: player, credit: credit})
  }

  creditFor(player) {
    return 100;
  }

  players() {
    return [...this._players];
  }
}

module.exports = Table;
